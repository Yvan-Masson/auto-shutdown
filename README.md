# auto-shutdown

`auto-shutdown` is a (quick-and-dirty) script meant to be run by cron to 
shutdown computer when users forgot to do it, for example at the end of the
day. If a user is still logged in, a pop-up appears so that the user can
eventually cancel the shutdown.

It should work on any supported Debian and derivative and is compatible with
Xorg and Wayland.


## Details:

Usage: `auto-shutdown [DELAY]`

Rules:

- If no user is connected, the computer shutdowns immediately;
- If more than one user is connected, nothing is done;
- If the user is connected in console (TTY, SSH), nothing is done;
- If the user is connected graphically, a dialog is displayed to let him abort
  the automatic shutdown. If the user does not answer before `DELAY` (in
  minutes, 2 by default), his session is closed and the computer shutdowns.

## Issues/TODO:

- There is no proper session closing, maybe we should do it.
- Session detection is 

## Dependencies:

This program relies on standard GNU/Linux tool that should already be
installed, except from:
- yad
