��          D      l       �      �   )   �   +   �     �   f       y  ;   �  -   �  �  �                          Automatic shutdown Computer will shutdown at $shutdown_time. Do not automatically shutdown, I will do it Usage: $script_name [DELAY]

This simple program is meant to be run from cron to shutdown the computer if
the users forgot to do it:
- If no user is connected, the computer shutdowns immediately;
- If more than one user is connected, nothing is done;
- If the user is connected in console (TTY, SSH), nothing is done;
- If the user is connected graphically, a dialog is displayed to let him abort
  the automatic shutdown. If the user does not answer before DELAY (in minutes,
  2 by default), his session is closed and the computer shutdowns. Project-Id-Version: 
Report-Msgid-Bugs-To: yvan@masson-informatique.fr
PO-Revision-Date: 2020-06-17 18:18+0100
Last-Translator: Yvan Masson <yvan@masson-informatique.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n > 1);
 Extinction automatique L'ordinateur s'éteindra automatiquement à $shutdown_time. Ne pas éteindre automatiquement, je le ferai Usage : $script_name [DÉLAI]

Ce programme basique est prévu pour être lancé par cron pour éteindre
l'ordinateur lorsque les utilisateurs ont oublié de le faire :
- Si aucun utilisateur n'est connecté, l'ordinateur est éteint immédiatement.
- Si plus d'un utilisateur est connecté, rien n'est fait.
- Si l'utilisateur est connecté en console (TTY, SSH), rien n'est fait.
- Si l'utilisateur est connecté graphiquement, un dialogue est affiché 
  pour lui permettre l'annuler l'extinction automatique. Si l'utilisateur 
  ne répond pas avant DELAI (en minutes, 2 par défaut), sa session est 
  fermée et l'ordinateur s'éteint. 